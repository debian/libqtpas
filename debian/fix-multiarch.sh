#!/bin/sh
# This script is required for Ubuntu-compatibility

for arg; do
  case $arg in
    HOST_MULTIARCH=*) DEB_HOST_MULTIARCH=${arg#HOST_MULTIARCH=};;
    TARGET=*) INSTALLDIR=${arg#TARGET=};;
  esac;
done

echo "Multiarch host: $DEB_HOST_MULTIARCH"
echo "Install target: $INSTALLDIR"

if [ ! -d "$INSTALLDIR/usr/lib/$DEB_HOST_MULTIARCH" ]; then
	echo "Moving files to multiarch path..."
	mkdir -p $INSTALLDIR/usr/lib/$DEB_HOST_MULTIARCH
	mv `find $INSTALLDIR/usr/lib '!' -type d | fgrep -v $DEB_HOST_MULTIARCH` $INSTALLDIR/usr/lib/$DEB_HOST_MULTIARCH/
fi
